import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { toast } from 'react-toastify';
import { postNewRecord } from '../../services/api';
import { connect } from 'react-redux';
import { fetchData } from '../../store/actions/index';

const useStyles = makeStyles(theme => ({
  
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    },
    paper: {
        width: 500,
        maxHeight: 700,

        backgroundColor: theme.palette.background.paper,
        border: '1px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 2, 3),
    },
   
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },


}));

const PaymentModal = ({ open, setOpen, onFetchData, onLoading }) => {
    const classes = useStyles();
    const [data, setData] = useState({
        name: '',
        phoneNumber: "",
        email: "",
        website: ""
    })
    const [isSubmit, setIsSubmit] = useState(false)
    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            setIsSubmit(true)
            if (!data.name || !data.phoneNumber || !data.email || !data.website)
                return
            onLoading(true)
            var dataReq = {
                name: data.name,
                phone_number: data.phoneNumber,
                email: data.email,
                website: data.website
            }
            await postNewRecord(dataReq)
            onLoading(false)
            onFetchData()
            handleClose()
            toast.success("New Record Addedd Successfully")
        } catch (err) {
            onLoading(false)
            toast.error(err.message)
        }
    }

    const handleChange = (e) => {
        setData({ ...data, [e.target.name]: e.target.value })
    }
    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
                disableScrollLock={false}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h3 id="transition-modal-title">
                            Add New Record
                        </h3>
                        <form onSubmit={handleSubmit} className={classes.form} noValidate>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="name"
                                label="Name"
                                name="name"
                                autoComplete="name"
                                autoFocus
                                error={isSubmit && !data.name}
                                value={data.name}
                                onChange={handleChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="phoneNumber"
                                label="Phone Number"
                                type="text"
                                id="phoneNumber"
                                autoComplete="phoneNumber"
                                error={isSubmit && !data.phoneNumber}
                                value={data.phoneNumber}
                                onChange={handleChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="email"
                                label="Email"
                                type="email"
                                id="email"
                                autoComplete="email"
                                error={isSubmit && !data.email}
                                value={data.email}
                                onChange={handleChange}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="website"
                                label="Website"
                                type="text"
                                id="website"
                                autoComplete="website"
                                error={isSubmit && !data.website}
                                value={data.website}
                                onChange={handleChange}
                            />

                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Submit
                            </Button>

                        </form>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchData: (url) => dispatch(fetchData(url)),
        onLoading: (payload) => dispatch({ type: "LOADING", payload })
    };
}

export default connect(null, mapDispatchToProps)(PaymentModal)