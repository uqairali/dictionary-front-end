import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { toast } from "react-toastify";
import { deleteRecord } from "../../services/api";
import { connect } from "react-redux";
import { fetchData } from "../../store/actions/index";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    width: 600,
    maxHeight: 700,

    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
  },

  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

const PaymentModal = ({
  open,
  setOpen,
  onFetchData,
  onLoading,
  selectedObj,
}) => {
  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = async () => {
    try {
      onLoading(true);
      await deleteRecord(selectedObj.id);
      onLoading(false);
      onFetchData();
      handleClose();
      toast.success("Record Deleted Successfully");
    } catch (err) {
      onLoading(false);
      toast.error(err.message);
    }
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
        disableScrollLock={false}
        fullWidth
      >
        <Fade in={open}>
          <div className={classes.paper}>
            {/* <h2 id="transition-modal-title">
                            Are your sure you want to delete record?
                        </h2> */}

            <Typography variant="h6" component="h6">
              {" "}
              Are your sure you want to delete record?
            </Typography>
            <div style={{ marginTop: "100px" }}>
              <Button
                variant="contained"
                color="secondary"
                className={classes.submit}
                onClick={handleClose}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => handleDelete()}
                style={{ marginLeft: "10px" }}
              >
                Confirm
              </Button>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchData: (url) => dispatch(fetchData(url)),
    onLoading: (payload) => dispatch({ type: "LOADING", payload }),
  };
};

export default connect(null, mapDispatchToProps)(PaymentModal);
