import { SwipeableDrawer } from "@material-ui/core";
import React from "react";

const SideDrawer = ({ open, onClose, onOpen, children, minWidth, padding }) => {
    return (
        <SwipeableDrawer
            anchor="left"
            open={Boolean(open)}
            onClose={onClose}
            onOpen={onOpen}
            className="drawer-container"
            transitionDuration={600}
            SlideProps={{ style: { minWidth: minWidth, padding: padding } }}
        >
            {children}
        </SwipeableDrawer>
    );
};

export default SideDrawer;
