import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {
  Language,
  Favorite,
  Delete,
} from "@material-ui/icons";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import PhoneEnabledIcon from "@material-ui/icons/PhoneEnabled";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import EditIcon from "@material-ui/icons/BorderColor";
import { profileSVG } from "../utils/const";
import UpdateModal from "./modals/updateRecord";
import { connect } from "react-redux";
import { fetchData } from "../store/actions/index";
import { toast } from "react-toastify";
import { updateRecord } from "../services/api";
import DeleteModal from "./modals/deleteRecord";

import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },

  avatar: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    height: "200px"
  },
  cardHeader: {
    backgroundColor: "#f5f5f5",
    width: "100%",
  },
  cardBody: {
    padding: "24px",
  },
  cardTitle: {
    fontSize: "18px",
  },
  cardBodyItems: {
    // display: "flex",
    // justifyContent: "space-around",
    // alignItems: "center",
    fontSize: "14px",
    lineHeight: "2",
    paddingLeft: "10px",
  },
  singleInfo: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap",
  },
  cardFooter: {
    margin: 0,
    padding: 0,
    listStyle: "none",
    background: "#fafafa",
    borderTop: "1px solid #e8e8e8",
    display: "flex",
    justifyContent: "space-around",
  },
  bodyIcon: {
    fontSize: "20px",
  },
  media: {
    width: 200,
    height: 200,
  },
}));

const MediaCard = ({ post, index, onLoading, onFetchData, loading }) => {
  const classes = useStyles();
  const [showEditModal, setShowEditModal] = useState(false);
  const [selectedObj, setSelectedObj] = useState({});
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const handleFavoriteClick = async () => {
    try {
      onLoading(true);
      var dataReq = {
        is_favorite: post.is_favorite ? 0 : 1,
      };
      await updateRecord(dataReq, post.id);
      onLoading(false);
      onFetchData();
    } catch (err) {
      onLoading(false);
      toast.error(err.message);
    }
  };

  return (
    <>
      <Grid item xs={12} sm={6} md={3} lg={3}>
        <Card className={classes.root}>
          <CardContent>
            <div className={classes.cardHeader}>
              <img src={profileSVG[+index[index.length - 1]]} className={classes.avatar} />
            </div>
            <div className={classes.cardBody}>
              <Typography
                variant="h6"
                component="h2"
                className={classes.cardTitle}
              >
                {post.name}
              </Typography>{" "}
              <div className={classes.singleInfo}>
                <MailOutlineIcon className={classes.bodyIcon} />
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  className={classes.cardBodyItems}
                >
                  {post.email}
                </Typography>
              </div>
              <div className={classes.singleInfo}>
                <PhoneEnabledIcon className={classes.bodyIcon} />
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  className={classes.cardBodyItems}
                >
                  {post.phone_number}
                </Typography>
              </div>
              <div className={classes.singleInfo}>
                <Language className={classes.bodyIcon} />
                <Typography
                  variant="body2"
                  color="textSecondary"
                  component="p"
                  className={classes.cardBodyItems}
                >
                  {post.website}
                </Typography>
              </div>
            </div>
          </CardContent>
          <CardActions disableSpacing className={classes.cardFooter}>
            <IconButton
              aria-label="Add to favorites"
              onClick={handleFavoriteClick}
            >
              {post.is_favorite === 1 ? (
                <Favorite color="secondary" fontSize="medium" />
              ) : (
                <FavoriteBorderIcon color="secondary" fontSize="medium" />
              )}
            </IconButton>
            <IconButton onClick={() => {
              setSelectedObj(post);
              setShowEditModal(true);
            }} aria-label="Edit">
              <EditIcon
                fontSize="medium"

              />
            </IconButton>
            <IconButton onClick={() => {
              setSelectedObj(post);
              setShowDeleteModal(true);
            }} aria-label="Delete">
              <Delete
                fontSize="medium"

              />
            </IconButton>
          </CardActions>
        </Card>
      </Grid>
      {showEditModal && (
        <UpdateModal
          open={showEditModal}
          setOpen={setShowEditModal}
          selectedObj={selectedObj}
        />
      )}
      {showDeleteModal && (
        <DeleteModal
          open={showDeleteModal}
          setOpen={setShowDeleteModal}
          selectedObj={selectedObj}
        />
      )}
    </>
  );
};

const mapStateToProps = (state) => {
  return { loading: state.loading };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchData: (url) => dispatch(fetchData(url)),
    onLoading: (payload) => dispatch({ type: "LOADING", payload }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(MediaCard);
