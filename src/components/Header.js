import React, { useContext, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Toolbar,
  Button,
  Box,
  Menu,
  MenuItem,
} from "@material-ui/core";
import Logo from "../assets/img/logo.jpg";
import { useHistory } from "react-router";
import SideDrawer from "../components/SideDrawer";
import { AuthContext } from "../context";
import { LOGOUT } from "../context/actionTypes";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import AddModal from "./modals/addNewRecord";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: 102,
  },
  logo: {
    maxWidth: 45,
    height: "auto",
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(1),
    cursor: "pointer",
  },
  toolbar: {
    [theme.breakpoints.down("sm")]: {
      justifyContent: "space-between",
    },
  },
  navitem: {
    textTransform: "capitalize",
  },
  leftItems: {
    flexGrow: 1,
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      flexDirection: "column",
      padding: "2rem 1rem",
    },
  },
  rightItems: {
    display: "flex",
  },
  meenu: {
    border: "1px solid red",
  },
  sideDrawerContainer: {
    padding: "1rem",
  },
}));

export default function Header() {
  const classes = useStyles();
  const history = useHistory();
  const [auth] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const { state, dispatch } = useContext(AuthContext);
  const [showAddModal, setShowAddNewModal] = useState(false);
  const [mobileScreen, setMobileScreen] = useState(false);
  const matches = useMediaQuery("(max-width:767.9px)");

  const handleMobileMenuClose = () => {
    setMobileScreen(false);
  };

  const handleMobileMenuOpen = (event) => {
    setMobileScreen(event.currentTarget);
  };

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <SideDrawer
      open={mobileScreen}
      onClose={handleMobileMenuClose}
      onOpen={() => setMobileScreen(true)}
    ></SideDrawer>
  );

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    dispatch({
      type: LOGOUT,
    });
    history.push("/login");
    handleClose();
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.header}>
        <Toolbar className={classes.toolbar}>
          <Box component="div" className={classes.leftItems}>
            <Button
              color="inherit"
              className={classes.navitem}
              onClick={() => setShowAddNewModal(true)}
            >
              Add new Record
            </Button>
          </Box>

          <Box component="div" className={classes.rightItems}>
            {auth && (
              <>
                <Button
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleMenu}
                  color="inherit"
                  className={classes.navitem}
                >
                  {`${state?.user?.name}`}
                </Button>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={open}
                  onClose={handleClose}
                  style={{ top: "30px" }}
                >
                  <MenuItem onClick={handleLogout}>Logout</MenuItem>
                </Menu>
              </>
            )}
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}

      {showAddModal && (
        <AddModal open={showAddModal} setOpen={setShowAddNewModal} />
      )}
    </div>
  );
}
