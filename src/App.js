import React, { useContext, Suspense, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Header from "./components/Header";
import Home from "./containers/Home";
import Login from "./containers/Login";
import Registration from "./containers/Registration";
import { AuthContext } from "./context";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import PageLoading from './components/pageLoader';

function App({ loading }) {
  const { state, setTokensFromLocalStorage } = useContext(AuthContext);
  useEffect(() => {
    var token = JSON.parse(localStorage.getItem("token"));
    setTokensFromLocalStorage(token)
  }, [])
  let routes = <></>
  if (!localStorage.getItem("token")) {
    routes = (
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/registration" component={Registration} />
        <Redirect to="/login" />
      </Switch>
    );
  }
  if (state.isAuthenticated) {
    routes = (
      <Switch>
        <Route exact path="/" component={Home} />
        <Redirect to="/" />
        {/* <Route path="*" component={NotFoundPage} /> */}
      </Switch>
    );
  }
  return (
    <>
      {loading && <PageLoading />}
      <ToastContainer />
      <Router>
        <Route
          exact
          path={[
            "/"
          ]}
          render={() => <Header />}
        />
        <Suspense fallback={<p>Loading...</p>}>{routes}</Suspense>
      </Router>
    </>
  );
}

const mapStateToProps = state => {
  return { loading: state.loading }
}


export default connect(mapStateToProps)(App);
