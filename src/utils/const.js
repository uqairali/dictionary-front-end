export const profileSVG = [
    "https://avatars.dicebear.com/v2/avataaars/Bret.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Antonette.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Samantha.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Karianne.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Kamren.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Leopoldo_Corkery.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Elwyn.Skiles.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Maxime_Nienow.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Delphine.svg?options[mood][]=happy",
    "https://avatars.dicebear.com/v2/avataaars/Moriah.Stanton.svg?options[mood][]=happy"
]