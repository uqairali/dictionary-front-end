import React, { useReducer, useEffect } from "react";
import { CHECK_USER, LOGIN, LOGOUT } from "./actionTypes";
import { setAxiosAuthorizationHeader } from '../axiosConfig';
export const AuthContext = React.createContext();
export const initialState = {
  isAuthenticated: false,
  user: null,
  token: null,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {

    case CHECK_USER:
      const user = localStorage.getItem("user");
      const token = localStorage.getItem("token");
      if (user && token) {
        return {
          ...state,
          isAuthenticated: true,
          user: JSON.parse(user),
          token: JSON.parse(token),
        };
      }

      return {
        ...state,
        isAuthenticated: false,
        user: null,
        token: null,
      };
    case LOGIN:
      localStorage.setItem("user", JSON.stringify(action.payload.user));
      localStorage.setItem("token", JSON.stringify(action.payload.accessToken));

      setTokensFromLocalStorage(action.payload.accessToken)
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        token: action.payload.accessToken,
      };
    case LOGOUT:
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };


    default:
      return state;
  }
};
const setTokensFromLocalStorage = (token) => {
  setAxiosAuthorizationHeader(token);
}

export const ContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  useEffect(() => {
    dispatch({
      type: CHECK_USER,
    });

  }, []);


  return (
    <AuthContext.Provider value={{ state, dispatch, setTokensFromLocalStorage }}>
      {children}
    </AuthContext.Provider>
  );
};
