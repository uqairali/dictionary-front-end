
const initialState = {
    posts: [],
    loading: false
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_CONTACTS":
            return {
                ...state,
                posts: action.payload

            }
        case "LOADING":
            return {
                ...state,
                loading: action.payload

            }


        default:
            return state;
    }
}

export default reducer