import resolve from "./resolve";
import axios from 'axios'
let apiBase = process.env.REACT_APP_API_BASEURL;

export const loginReq = async (req) => {
    return await resolve(axios.post(apiBase + "user/login", req).then(res => res.data))
}
export const registrationReq = async (req) => {
    return await resolve(axios.post(apiBase + "user/register", req).then(res => res.data))
}

export const postNewRecord = async (req) => {
    return await resolve(axios.post(apiBase + "dictionary/newEntry", req).then(res => res.data))
}
export const fetchAllRecord = async () => {
    return await resolve(axios.get(apiBase + "dictionary/findAll").then(res => res.data))
}

export const updateRecord = async (data,id) => {
    return await resolve(axios.put(apiBase + `dictionary/update/${id}`,data).then(res => res.data))
}

export const deleteRecord = async (id) => {
    return await resolve(axios.delete(apiBase + `dictionary/delete/${id}`).then(res => res.data))
}