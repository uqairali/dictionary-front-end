import React, { useEffect } from "react";
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import ContactCard from '../../components/contactCard';
import { fetchData } from '../../store/actions/index';

const Home = ({ posts, onFetchData }) => {
  useEffect(() => {
    onFetchData()
  }, [])
  return (
    <div className="home-container">
      <Grid
        container
        spacing={3}
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        {posts.map((post, key) => (
          <ContactCard post={post} index={key.toString()} />
        ))}
      </Grid>
    </div>
  );
};


const mapStateToProps = state => {
  return { posts: state.posts }
}

const mapDispatchToProps = dispatch => {
  return {
    onFetchData: (url) => dispatch(fetchData(url))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);

